<!DOCTYPE html>

<head>
	<title>Calculate Electricity Bill</title>

<style>
body{
    text-align:center;
    
    padding:16px;
}
div{
	border:1px  solid black;
    width:350px;
    height:500px;
	text-align:center;
	background-color:rgba(0,0,0,0.4);
	padding:5px;
	margin-left:330px;
	color:white;

}
h1{
	color:white;
    text-decoration:underline;
}
input{
    padding:10px;
    margin:10px;
    background-color:rgba(0,0,0,0.1);
    color:white;
}
.in{
    margin-top:80px;
}
.btn{
    border:1px solid;
    width:100px;
    height:50px;
    border-radius:5px;
    color:white;
}
.bi{
	background-image:url(img/p16.jpg);
	background-size:cover;
	
  }
}
</style>
</head>

<?php
$result_str = $result = '';
if (isset($_POST['unit-submit'])) {
    $units = $_POST['units'];
    if (!empty($units)) {
        $result = calculate_bill($units);
        $result_str = 'Total amount of ' . $units . ' - ' . $result;
    }
}
/**
 * To calculate electricity bill as per unit cost
 */
function calculate_bill($units) {
    $unit_cost_first = 1.50;
    $unit_cost_second = 2.90;
    $unit_cost_third = 3.40;
    $unit_cost_fourth = 4.50;
	 $unit_cost_fifth = 6.10;
	 $unit_cost_sixth = 7.30;
	 

    if($units <= 40) {
        $bill = $units * $unit_cost_first;
    }
    else if($units > 50 && $units <= 100) {
        $temp = 50 * $unit_cost_first;
        $remaining_units = $units - 50;
        $bill = $temp + ($remaining_units * $unit_cost_second);
    }
    else if($units > 100 && $units <= 150) {
        $temp = (50 * 3.4) + (100 * $unit_cost_second);
        $remaining_units = $units - 150;
        $bill = $temp + ($remaining_units * $unit_cost_third);
    }
	
    else if($units > 150 && $units <= 200) {
        $temp = (50 * 4.5) + (100 * $unit_cost_second) + (100 * $unit_cost_third);
        $remaining_units = $units - 200;
        $bill = $temp + ($remaining_units * $unit_cost_fourth);
		
    }
	 else if($units > 150 && $units <= 250) {
        $temp = (50 * 6.1) + (100 * $unit_cost_second) + (100 * $unit_cost_third)+(100 * $unit_cost_fourth);
        $remaining_units = $units - 250;
        $bill = $temp + ($remaining_units * $unit_cost_fifth);
		
    }
	else
	{
		$temp = (50 * 7.3) + (100 * $unit_cost_second) + (100 * $unit_cost_third)+(100 * $unit_cost_fourth)+(100 * $unit_cost_fifth);
        $remaining_units = $units - 250;
        $bill = $temp + ($remaining_units * $unit_cost_sixth);
	}
	
    return number_format((float)$bill, 2, '.', '');
}

?>

<body class="bi">
	<form action="invoice.php" method="post" id="quiz-form">
	<div>
		<h1>Calculate Electricity Bill</h1>

		

Tariff:<input list="tariff" name="tariff" placeholder=""></br>
<datalist id="tariff">
<option value="LT-1A">
<option value="LT-2A">
<option value="LT-3A">
<option value="Other">
</datalist></br>
Purpose:<input list="purpose" name="purpose" placeholder="">
<datalist id="purpose">
<option value="Domestic">
<option value="Industrial">
<option value="Other">
</datalist></br></br>
Billing Cycle<input type="radio" name="month" id="2 month">
<label for="2 month">2 month</label>
<input type="radio" name="month" id="1 month">
<label for="1 month">1 month</label></br>
Consumed Units<input type="number" name="units" id="units" placeholder="Please enter no. of Units" /></br></br>
Phase	<input type="radio" name="phase" id="Single Phase">
<label for="Single Phase">Single Phase</label>
<input type="radio" name="phase" id="Three Phase">
<label for="Three Phase">Three Phase</label></br></br>			
        <input type="submit" name="unit-submit" id="unit-submit" value="Submit" />
		
		</form>
		<?php echo '<br />' . $result_str; ?>
</div>
	<!---<div>
		    
		</div> -->
<a href="index.html">Back To Home</a>	
</body>
</html>